<?php

include_once('init.php');
$url = parseUrl($_GET['querysistemurl'] ?? '');

$cname = $url[0] ?? 'index';
$path = "controllers/$cname.php";
$pageTitle = 'Ошибка 404';
$pageContent = '';

if(checkControllerName($cname) && file_exists($path)){
	include_once($path);
}
else{
	$pageContent = template('errors/v_404');
}


