<?php
//var_dump($_POST);
include_once('init.php');

ob_start();
$query = dbQuery("SELECT * FROM `cerere_neprocesata` ORDER BY `creation_date` DESC", []);
$cereri_neprocesate = $query->fetchAll();

$query1 = dbQuery("SELECT * FROM `cereri_procesate` ORDER BY `date_of_procesing` DESC", []);
$cereri_procesate = $query1->fetchAll();
	//print_r($cereri_procesate);



$pageTitle = 'Cereri neprocesate';
$error = validateFormAdminPage();
$html = template('admin/v_admin_page', [
	'pageTitle' => $pageTitle,
	'cereri_neprocesate' => $cereri_neprocesate,
	'cereri_procesate' => $cereri_procesate,
	'error' => $error
	]);

echo $html;

if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['avocat'] === "null"  && $_POST['delete'] === "delete")  {
	dbQuery("DELETE FROM `cerere_neprocesata` WHERE `cerere_neprocesata`.`id_cerere_neprocesata` = :id", ["id" => $_POST['id_cerere']]);
	
	header('Location:' . BASE_URL . 'admin_page');
	
	exit;
}
elseif ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['client'] !== null && $_POST['avocat'] !== null)  {
	$query1 = dbQuery("SELECT id_problema,descriere_problema  FROM `cerere_neprocesata` WHERE id_client = :id_client", ['id_client' => $_POST['client']]);
	$result = $query1->fetch();
	

	dbQuery("INSERT INTO `cereri_procesate`(`id_client`, `id_avocat`, `id_problema`, `descriere_problema`) 
				VALUES (:id_client,:id_avocat,:id_problema,:descriere_problema )",
	["id_client" => $_POST['client'], 
	'id_avocat' => $_POST['avocat'],
	'id_problema' => $result['id_problema'],
	'descriere_problema' => $result['descriere_problema']
]);
	dbQuery("DELETE FROM `cerere_neprocesata` WHERE `cerere_neprocesata`.`id_cerere_neprocesata` = :id", ["id" => $_POST['id_cerere']]);
	header('Location:' . BASE_URL . 'admin_page');
	exit;
}

ob_end_flush();
//var_dump($_POST);
// var_dump($result);

?>
<?if ($error === "Alegeti un avocat puntru clientul dat!!") :?>
	<script>
		alert("<?=$error?>");
	</script>
	<?exit;?>
<?endif;?>

	
