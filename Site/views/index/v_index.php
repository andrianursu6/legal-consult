<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

	<title><?=$pageTitle?></title>

	<!-- Loading third party fonts -->
	<link href="<?=BASE_URL?>assets/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=BASE_URL?>assets/fonts/novecento-font/novecento-font.css" rel="stylesheet">

	<!-- Loading main css file -->
	<link href="<?=BASE_URL?>assets/css/style.css" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>


<body>

	<div id="site-content">

		<header class="site-header">
			<div class="container">
				<a href="<?=BASE_URL?>" id="branding">
					<img src="assets/images/logo.png" alt="Company Name" class="logo">
					<div class="branding-copy">
						<h1 class="site-title">Legal Consultation</h1>
						<small class="site-description">FOR YOU</small>
					</div>
				</a>

				<nav class="main-navigation">
					<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
					<ul class="menu">
						<li class="menu-item current-menu-item"><a href="<?=BASE_URL?>">Acasa</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>about">Despre/Service</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>contacts">Contacte</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>login">Autentificați-vă</a></li>
					</ul>
				</nav>
				<nav class="mobile-navigation"></nav>
			</div>
		</header> <!-- .site-header -->

		

		<div class="fullwidth-block" data-bg-color="#111113">
			<div class="container">
				<div class="row">
					<div class="col-md-8">  
						<h2>BINE ATI VENIT PE SITE-UL NOSTRU</h2>
						<p>Biroul nostru este fondat în anul 2011 și activează pe piața avocaturii
							mai bine de 10 ani.
							Cabinetul nostru de avocatură are menirea exclusivă de a vă ajuta în orice problemă juridică
							apărută; de a vă consulta,
							reprezenta, asista în chestiuni juridice de orice natură, în fața oricăror instituții, organizații,
							întreprinderi
							naționale și internaționale indiferent de forma de proprietate a lor; în fața organelor ale
							gestiunii administrative și
							de stat a Republicii Moldova și a altor state ca subiecte de drept internațional, precum și în fața
							organelor de
							poliție, ale procuraturii, instanțelor de judecată, instituțiilor de executare naționale și din
							alte state, etc., cu
							toate împuternicirile reprezentantului prevăzute de legislația în vigoare a Republicii Moldova.</p>
					</div>
					
				</div>
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- .fullwidth-block -->

	<div class="fullwidth-block">
		<div class="container">
			<div class="row feature-list-section">
				<div class="col-md-4">
					<div class="feature">
						<header>
							<img src="assets/images/icon-1.png" class="feature-icon">
							<div class="feature-title-copy">
								<h2 class="feature-title">Cauze penale</h2>
								<small class="feature-subtitle">Acordăm consultanță</small>
							</div>
						</header>
						<p>În domeniul dreptului penal noi acordăm consultanță și reprezentare pe următoarele problematici, și
							anume:

							întocmiri de cereri, demersuri, plîngeri, etc.
							întocmiri de acțiuni civile (recuperare prejudicii și daune interese) în cauze penale.
							reprezentare și asistentă în fața organelor de urmărire penală.
							înaintarea de obiecţii împotriva acţiunilor organului de urmărire penală.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature">
						<header>
							<img src="assets/images/icon-2.png" class="feature-icon">
							<div class="feature-title-copy">
								<h2 class="feature-title">IMOBIL</h2>
								<small class="feature-subtitle">Tranzacțiile de vânzare-cumpărare</small>
							</div>
						</header>
						<p>Tranzacțiile de vânzare-cumpărare se numără printre cele mai complexe și responsabile afaceri din
							viața oricărei
							persoane. Pentru a nu greși și a nu rămâne dezamăgiți în urma tranzacției de vânzare-cumpărare a
							bunurilor imobiliare
							sau de închiriere a unei proprietăți, ar fi bine să consultați un avocat sau un notar. Ei vă vor
							ajuta să înțelegeți mai
							bine aspectele juridice.
							!</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature">
						<header>
							<img src="assets/images/icon-3.png" class="feature-icon">
							<div class="feature-title-copy">
								<h2 class="feature-title">Munca</h2>
								<small class="feature-subtitle">Oferte de muncă peste hotare</small>
							</div>
						</header>
						<p>În domeniul migrației de muncă noi acordăm consultanță și reprezentare pe următoarele problematici,
							și anume:

							analiza ofertelor de muncă peste hotare acordate de agențiile private de ocupare a forței de muncă
							din Republica
							Moldova.
							consultanță și asistență la încheierea contractelor de mediere privind plasarea la muncă în
							străinătate și a altor acte
							juridice.</p>
					</div>
				</div>
			</div>

			<div class="quote-section client-form">

			<div class="client-form-title">
				Solutioneaza orice problema cu un avocat al companiei noastre
			</div>
				<form class="client-form" method="post">
					<label for="category">Categoria problemei dvs.</label>
						<select name="category" id="category" >
							<option value="1">Consultatie</option>
							<option value="2">Servicii publice</option>
							<option value="3">Afaceri</option>
							<option value="4">Cauze Penale</option>
							<option value="5">Imobil</option>
							<option value="6">Recuperare datorii</option>
							<option value="7">Compensare daune</option>
							<option value="8">Despagubiri</option>
						</select>
					
					<label for="category-description">In ce consta problema dvs? Descrieti-o cat mai detaliat</label>
					<textarea name="category_description" id="" cols="30" rows="10" required><?=$_POST['category_description']?></textarea>
<br>
					<label for="nume">Numele: </label>
					<input type="text" name="nume" value="<?=$_POST['nume']?>" required>
					
					<label for="name">Prenumele: </label>
					<input type="text" name="prenume" value="<?=$_POST['prenume']?>" required>
					
					<label for="telephone">Numarul dvs. de telefon: </label>
					<input type="tel" name="telephone" value="<?=$_POST['telephone']?>" required>
					
					<label for="price">Emailul dvs.</label>
					<input type="text" name="email" value="<?=$_POST['email']?>" required>
					
					<label for="adress">Adresa dvs.</label>
					<input type="text" name="adress" value="<?=$_POST['adress']?>" required>

					<br>
					<label for="pas1">Introduceti parola: </label>
					<input type="password" name="pas1" value="<?=$_POST['pas1']?>" required>
					
					<label for="pas2">Reintroduceti parola: </label>
					<input type="password" name="pas2" value="<?=$_POST['pas2']?>" required>
					
				<button>Trimite</button>
			</form>
			<div class="error"><?=$error?></div>
			</div>
		</div>
	</div>

	</main> <!-- .main-content -->

	<footer class="site-footer">
		<div class="container">
			<div class="subscribe-form">
				<form action="#">
					<label for="#">
						<span>DORIȚI SĂ PRIMI ȘTIRI?</span>
						<span>ÎNSCRIEȚI-VĂ LA STIRELE NOASTRE</span>
					</label>
					<div class="control">
						<input type="text" placeholder="Introduceți adresa de e-mail a  dvs. pentru a vă abona ...">
						<button type="submit"><img src="assets/images/icon-envelope.png" alt=""></button>
				</form>
			</div>
		</div>
		<div class="social-links">
			<a href="<?=BASE_URL?>"><i class="fa fa-facebook"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-twitter"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-google-plus"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-pinterest"></i></a>
		</div>
		<div class="copy">
			<p>Copyright 2021 iLegal Consultation. All rights reserved.</p>
		</div>
		</div>
	</footer> <!-- .site-footer -->

	</div>
<script>
	 
		var id = "<?=$_POST['category']?>";
		var x = document.getElementsByTagName("option");
		for (let i = 0; i < x.length; i++) {
			if (id == i+1) {
				x[i].selected = true;
			}
		}
	
</script>

	<script src="<?=BASE_URL?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=BASE_URL?>assets/js/plugins.js"></script>
	<script src="<?=BASE_URL?>assets/js/app.js"></script>
	
</body>

</html>