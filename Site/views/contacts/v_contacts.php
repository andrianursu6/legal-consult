<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

	<title><?=$pageTitle?></title>

	<!-- Loading third party fonts -->
	<link href="<?=BASE_URL?>assets/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=BASE_URL?>assets/fonts/novecento-font/novecento-font.css" rel="stylesheet">

	<!-- Loading main css file -->
	<link href="<?=BASE_URL?>assets/css/style.css" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>


<body>

	<div id="site-content">

		<header class="site-header">
			<div class="container">
				<a href="<?=BASE_URL?>" id="branding">
					<img src="assets/images/logo.png" alt="Company Name" class="logo">
					<div class="branding-copy">
						<h1 class="site-title">Legal Consultation</h1>
						<small class="site-description">FOR YOU</small>
					</div>
				</a>

				<nav class="main-navigation">
					<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
					<ul class="menu">
						<li class="menu-item "><a href="<?=BASE_URL?>">Acasa</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>about">Despre/Service</a></li>
						<li class="menu-item current-menu-item"><a href="<?=BASE_URL?>contacts">Contacte</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>login">Autentificați-vă</a></li>
					</ul>
				</nav>
				<nav class="mobile-navigation"></nav>
			</div>
		</header> <!-- .site-header -->

		

		<div class="fullwidth-block" data-bg-color="#111113">
			<div class="container">
				<div class="row">
					<div class="col-md-8">  
						<h2>BINE ATI VENIT PE SITE-UL NOSTRU</h2>
						<p>Biroul nostru este fondat în anul 2011 și activează pe piața avocaturii
							mai bine de 10 ani.
							Cabinetul nostru de avocatură are menirea exclusivă de a vă ajuta în orice problemă juridică
							apărută; de a vă consulta,
							reprezenta, asista în chestiuni juridice de orice natură, în fața oricăror instituții, organizații,
							întreprinderi
							naționale și internaționale indiferent de forma de proprietate a lor; în fața organelor ale
							gestiunii administrative și
							de stat a Republicii Moldova și a altor state ca subiecte de drept internațional, precum și în fața
							organelor de
							poliție, ale procuraturii, instanțelor de judecată, instituțiilor de executare naționale și din
							alte state, etc., cu
							toate împuternicirile reprezentantului prevăzute de legislația în vigoare a Republicii Moldova.</p>
					</div>
					
				</div>
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- .fullwidth-block -->

>

	</main> <!-- .main-content -->

	<footer class="site-footer">
		<div class="container">
			<div class="subscribe-form">
				<form action="#">
					<label for="#">
						<span>DORIȚI SĂ PRIMI ȘTIRI?</span>
						<span>ÎNSCRIEȚI-VĂ LA STIRELE NOASTRE</span>
					</label>
					<div class="control">
						<input type="text" placeholder="Introduceți adresa de e-mail a  dvs. pentru a vă abona ...">
						<button type="submit"><img src="assets/images/icon-envelope.png" alt=""></button>
				</form>
			</div>
		</div>
		<div class="social-links">
			<a href="<?=BASE_URL?>"><i class="fa fa-facebook"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-twitter"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-google-plus"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-pinterest"></i></a>
		</div>
		<div class="copy">
			<p>Copyright 2021 iLegal Consultation. All rights reserved.</p>
		</div>
		</div>
	</footer> <!-- .site-footer -->

	</div>


	<script src="<?=BASE_URL?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=BASE_URL?>assets/js/plugins.js"></script>
	<script src="<?=BASE_URL?>assets/js/app.js"></script>
	
</body>

</html>