<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

	<title><?=$pageTitle?></title>

	<!-- Loading third party fonts -->
	<link href="<?=BASE_URL?>assets/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=BASE_URL?>assets/fonts/novecento-font/novecento-font.css" rel="stylesheet">

	<!-- Loading main css file -->
	<link href="<?=BASE_URL?>assets/css/style.css" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>


<body>

	<div id="site-content">

		<header class="site-header">
			<div class="container">
				<a href="<?=BASE_URL?>" id="branding">
					<img src="<?=BASE_URL?>assets/images/logo.png" alt="Company Name" class="logo">
					<div class="branding-copy">
						<h1 class="site-title">Legal Consultation</h1>
						<small class="site-description">FOR YOU</small>
					</div>
				</a>

				<nav class="main-navigation">
					<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
					<ul class="menu">
						<li class="menu-item "><a href="<?=BASE_URL?>">Acasa</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>about">Despre/Service</a></li>
						<li class="menu-item "><a href="<?=BASE_URL?>contacts">Contacte</a></li>
						<li class="menu-item current-menu-item"><a href="<?=BASE_URL?>login">Autentificați-vă</a></li>
					</ul>
				</nav>
				<nav class="mobile-navigation"></nav>
			</div>
		</header> <!-- .site-header -->

		

		<div class="fullwidth-block" data-bg-color="#111113">
			<div class="container">
				<div class="product-details-tabs">
					<div class="product-details-tabs-btn">
							<div class="quote-section admin-form">
								<div class="client-form-title">
									Autentificați-va ca <?=$pageTitle?>
								</div>
								<form class="client-form" method="post">
									<input type="text" name="user" value="<?=$_POST['user']?>" placeholder="Email or username:" required>
									<br>
									<input type="password" name="pas" value="<?=$_POST['pas']?>" placeholder="Email or username:" required>
									<button>Login</button>
								</form>
								<div class="error"><?=$error?></div>
								<div class="admin-form-links">
									<a href="<?=BASE_URL?>login">Back..</a>
									<a href="<?=BASE_URL?>login-as/<?=$link?>">Intra ca <?=$link?></a>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- .fullwidth-block -->

	
						
	<footer class="site-footer">
		<div class="container">
			<div class="subscribe-form">
				<form action="#">
					<label for="#">
						<span>DORIȚI SĂ PRIMI ȘTIRI?</span>
						<span>ÎNSCRIEȚI-VĂ LA STIRELE NOASTRE</span>
					</label>
					<div class="control">
						<input type="text" placeholder="Introduceți adresa de e-mail a  dvs. pentru a vă abona ..." required>
						<button type="submit"><img src="<?=BASE_URL?>assets/images/icon-envelope.png" alt=""></button>
				</form>
			</div>
		</div>
		<div class="social-links">
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-google-plus"></i></a>
			<a href="#"><i class="fa fa-pinterest"></i></a>
		</div>
		<div class="copy">
			<p>Copyright 2021 iLegal Consultation. All rights reserved.</p>
		</div>
		</div>
	</footer> <!-- .site-footer -->

	</div>


	<script src="<?=BASE_URL?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=BASE_URL?>assets/js/plugins.js"></script>
	<script src="<?=BASE_URL?>assets/js/app.js"></script>
	<script src="<?=BASE_URL?>assets/js/tab.js"></script>
	
</body>

</html>