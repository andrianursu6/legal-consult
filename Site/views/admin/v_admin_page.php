<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

	<title>
		<?=$pageTitle?>
	</title>

	<!-- Loading third party fonts -->
	<link href="<?=BASE_URL?>assets/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=BASE_URL?>assets/fonts/novecento-font/novecento-font.css" rel="stylesheet">

	<!-- Loading main css file -->
	<link href="<?=BASE_URL?>assets/css/style.css" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>


<body>

	<div id="site-content">

		<header class="site-header">
			<div class="container">
				<a href="index" id="branding">
					<img src="assets/images/logo.png" alt="Company Name" class="logo">
					<div class="branding-copy">
						<h1 class="site-title">Legal Consultation</h1>
						<small class="site-description">FOR YOU</small>
					</div>
				</a>

				<nav class="main-navigation">
					<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
					<ul class="menu">
						<li class="menu-item "><a href="<?=BASE_URL?>">Acasa</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>about">Despre/Service</a></li>
						<li class="menu-item "><a href="<?=BASE_URL?>contacts">Contacte</a></li>
						<li class="menu-item current-menu-item"><a href="<?=BASE_URL?>login">Autentificați-vă</a></li>
					</ul>
				</nav>
				<nav class="mobile-navigation"></nav>
			</div>
		</header> <!-- .site-header -->




		<div class="fullwidth-block" data-bg-color="#111113">
			<div class="container">
				<div class="product-details-tabs">
					<div class="product-details-tabs-btn">
						<button class="tab-btn btn-active" onclick="tabShowContent('tab-notprocesed')">Cereri
							neprocesate</button>
						<button class="tab-btn" onclick="tabShowContent('tab-procesed')">Cereri procesate</button>
					</div>
					<div class="product-details-tabs-content ">
						<div class="tab-content tab-active" id="tab-notprocesed">
							<div class="title-admin">
								Alegeti unul din avocatii care este specializiat pe tipul problemei indicate de client
							</div>
							<?foreach ($cereri_neprocesate as $query_row) :?>
							<div class="cerere">
								<form class="cerere__aproba" method="post">
									<div class="cerere__aproba-inner">
										<div class="client">
											<?
										$query1 = dbQuery("SELECT * FROM `clienti` Where `id_client` = :client", [ 'client' => $query_row['id_client']]);
										$client = $query1->fetch();

										$query2 = dbQuery("SELECT * FROM `probleme` Where `id_problema` = :problema", [ 'problema' => $query_row['id_problema']]);
										$problema = $query2->fetch();

										$client_info = "<span>Clientul: </span> " . $client['nume'] . " " . $client['prenume'] . ".
										</br><span>Cu categoria problemei : </span>" .  strtoupper(trim($problema['nume_problema'])) . ".
										<br><span>Descrierea problemei : </span>" . $query_row['descriere_problema'] . "
										<br><span>Data creari cererii: : </span>" . $query_row['creation_date'];;


									?>
											<label for="client">
												<?=$client_info?>
											</label>
											<input type="radio" id="client" name="client" value="<?=$client['id_client']?>"
												checked>

										</div>
										<div class="avocat">
											<?
										$query3 = dbQuery("SELECT * FROM `avocati` Where `id_problema` = :problema", [ 'problema' => $query_row['id_problema']]);
										$avocati = $query3->fetchAll();
										//var_dump($avocati);
										
											?>
											<??>
											<div class="avocat__title"></div>
											<?foreach ($avocati as $avocat) :?>
											<?$name = "avocat";?>

											<?$avocat_info = "<p><span>Avocat: </span> " . $avocat['nume'] . " " . $avocat['prenume'] . "</p>"?>

											<label for="<?=$name?>">
												<?=$avocat_info?>
												<input type="radio" id="<?=$name?>" name="<?=$name?>"
													value="<?=$avocat['id_avocat']?>">
											</label required>

											<?//var_dump($_POST['avocat']);?>
											<?endforeach;?>


										</div>
									</div>
									<input id="id_cerere" name="id_cerere" type="hidden"
										value="<?=$query_row['id_cerere_neprocesata']?>">
									<div class="btn">
										<button type="submit">Aproba cerere</button>
									</div>
								</form>
								<form class="cerere-delete" method="post">
									<input id="avocat" name="avocat" type="hidden" value="null">
									<input id="id_cerere" name="id_cerere" type="hidden"
										value="<?=$query_row['id_cerere_neprocesata']?>">
									<input id="delete" name="delete" type="hidden" value="delete">
									<div class="btn">
										<button type="submit">Respinge cerere</button>
									</div>
								</form>
								<?
								// var_dump($query_row);
								// echo "<br>";
								// var_dump($_POST);
								// if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['client'] !== null && $_POST['avocat'] !== null &&  $query_row['id_problema'] !== null $query_row['descriere_problema'] !== null)  {
								// 	dbQuery("INSERT INTO `cereri_procesate`(`id_client`, `id_avocat`, `id_problema`, `descriere_problema`) 
								// 				VALUES (:id_client,:id_avocat,:id_problema,:descriere_problema )",
								// 	["id_client" => $_POST['client'], 
								// 	'id_avocat' => $_POST['avocat'],
								// 	'id_problema' => $query_row['id_problema'],
								// 	'descriere_problema' => $query_row['descriere_problema']
								// ]);
								// 	//dbQuery("DELETE FROM `cerere_neprocesata` WHERE `cerere_neprocesata`.`id_cerere_neprocesata` = :id", ["id" => $_POST['id_cerere']]);	
								// }
								?>
							</div>
							
							<?endforeach;?>

						</div>
						<div class="tab-content" id="tab-procesed">
							<div class="title-admin">
								Cereri procesate.
							</div>
							<div class="cerere">
								<table class="cerere__neprocesata">
									<tr>
										<th>Nume</th>
										<th>Prnume</th>
										<th>Telefon</th> 
										<th>Email</th>
										<th>Id Avocat</th>
										<th>Descrierea problemei</th>
										<th>Data procesarii</th>
										
									</tr>
									
									<?foreach ($cereri_procesate as $cerere_procesata) : ?>
										<tr>
											
											<?
											$queryInfoClient = dbQuery("SELECT  `nume`, `prenume`, `telefon`, `email`, `adresa` FROM
											`clienti` WHERE id_client = :id_client", [ 'id_client' => $cerere_procesata['id_client']]);
											$infoClient = $queryInfoClient->fetch();
											
											?>
										
											
											<td><?=$infoClient['nume'] ?></td>
											<td><?=$infoClient['prenume']?></td>
											<td><?=$infoClient['telefon'] ?></td> 
											<td><?=$infoClient['email'] ?></td>
											<td><?=$cerere_procesata['id_avocat'] ?></td>
											<td class="box-description"><?=$cerere_procesata['descriere_problema'] ?></td>
											<td><?=$cerere_procesata['date_of_procesing'] ?></td>
											

										</tr>
									<?endforeach;?>
								</table>
								
							</div>
						</div>
					</div>
				</div>



			</div> <!-- .row -->


		</div><!-- .container -->
	</div> <!-- .fullwidth-block -->



	<footer class="site-footer">
		<div class="container">
			<div class="subscribe-form">
				<form action="#">
					<label for="#">
						<span>DORIȚI SĂ PRIMI ȘTIRI?</span>
						<span>ÎNSCRIEȚI-VĂ LA STIRELE NOASTRE</span>
					</label>
					<div class="control">
						<input type="text" placeholder="Introduceți adresa de e-mail a  dvs. pentru a vă abona ..." required>
						<button type="submit"><img src="<?=BASE_URL?>assets/images/icon-envelope.png" alt=""></button>
				</form>
			</div>
		</div>
		<div class="social-links">
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-google-plus"></i></a>
			<a href="#"><i class="fa fa-pinterest"></i></a>
		</div>
		<div class="copy">
			<p>Copyright 2021 iLegal Consultation. All rights reserved.</p>
		</div>
		</div>
	</footer> <!-- .site-footer -->

	</div>


	<script src="<?=BASE_URL?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=BASE_URL?>assets/js/plugins.js"></script>
	<script src="<?=BASE_URL?>assets/js/app.js"></script>
	<script src="<?=BASE_URL?>assets/js/tab.js"></script>
</body>

</html>