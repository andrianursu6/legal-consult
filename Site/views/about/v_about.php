<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title><?=$pageTitle?></title>
		<!-- Loading third party fonts -->
		
	<!-- Loading third party fonts -->
	<link href="<?=BASE_URL?>assets/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=BASE_URL?>assets/fonts/novecento-font/novecento-font.css" rel="stylesheet">

	<!-- Loading main css file -->
	<link href="<?=BASE_URL?>assets/css/style.css" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>


	<body>
		
		<div id="site-content">
			
			<header class="site-header">
				<div class="container">
					<a href="index" id="branding">
						<img src="<?=BASE_URL?>assets/images/logo.png" alt="Company Name" class="logo">
						<div class="branding-copy">
							<h1 class="site-title">Legal Consultation</h1>
						<small class="site-description">FOR YOU</small>
						</div>
					</a>

					<nav class="main-navigation">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<nav class="main-navigation">
					<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
					<ul class="menu">
						<li class="menu-item "><a href="<?=BASE_URL?>">Acasa</a></li>
						<li class="menu-item current-menu-item"><a href="<?=BASE_URL?>about">Despre/Service</a></li>
						<li class="menu-item"><a href="<?=BASE_URL?>contacts">Contacte</a></li>	
						<li class="menu-item"><a href="<?=BASE_URL?>login">Autentificați-vă</a></li>

					</ul>
				</nav>
						</ul>
					</nav>
					<nav class="mobile-navigation"></nav>
				</div>
			</header> <!-- .site-header -->

			<main class="main-content">
				
				<div class="fullwidth-block content">
					<div class="container">
						<h2 class="entry-title">Competenţa profesională,Confidențialitate si Legalitate</h2>
						<p>Activitatea noastră se realizează prin acordarea consultațiilor şi interpretărilor juridice, expunerea concluziilor cu privire la problemele de drept, prezentarea informaţiilor verbale şi scrise referitoare la legislaţie, asistența sau reprezentarea în fața instanțelor de judecată ori a altor organe de ocrotire a normelor de drept, precum și persoanelor private, întocmirea documentelor cu caracter juridic.</p>
						
						
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-1.jpg" alt="person-1"></figure>
							<h3 class="team-name">Petran Evdokiya</h3>
							<small class="team-desc">Avocat</small>
							<p>În domeniul serviciilor publice va pot acorda consultanță juridică și reprezentare in diferite domenii.</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-2.jpg" alt="person-2"></figure>
							<h3 class="team-name">Bogdanov Sofron</h3>
							<small class="team-desc">Avocat</small>
							<p>În domeniul despăgubiri accidente rutiere, va pot acorda consultanță juridică și reprezentare in diferite domenii.</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-3.jpg" alt="person-3"></figure>
							<h3 class="team-name">Cojocaru Demid</h3>
							<small class="team-desc">Avocat</small>
							<p>În domeniul recuperării datorii (creanțe), va pot acorda consultanță juridică și reprezentare in diferite domenii.</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-4.jpg" alt="person-4"></figure>
							<h3 class="team-name">Kovalev Virginia</h3>
							<small class="team-desc">Avocat</small>
							<p>În acest domeniu, va pot acorda consultanță juridică și reprezentare pe următoarea problrma, și anume: compensarea daunelor materiale și morale, etc.

</p>
						</div>

						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-5.jpg" alt="person-5"></figure>
							<h3 class="team-name">Nicolescu Radu</h3>
							<small class="team-desc">Avocat</small>
							<p>Pentru diverse problematici și întrebări de ordin juridic cu complexitate moderată, va pot acorda gratuit: consultanță juridică on-line si consultanță juridică telefonică.
</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-6.jpg" alt="person-6"></figure>
							<h3 class="team-name">Popescu Augustin</h3>
							<small class="team-desc">Avocat</small>
							<p>În domeniul serviciilor imobiliare, va pot acorda consultanță juridică și reprezentare pe următoarele problematici, și anume:

elaborare de contracte, alte acte juridice privitor la: vanzarea-cumpararea bunurilor imobile;  arendă de spații comerciale, etc.</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-7.jpg" alt="person-7"></figure>
							<h3 class="team-name">Ivanov Renat</h3>
							<small class="team-desc">Avocat</small>
							<p>De multe ori planurile ori proiectele de afaceri ale unor companii riscă să depășească cadrul legal, prin urmare, activitatea acestora pot fi în contradicţie cu legile penale. Pentru prevenirea unor astfel de situaţii, noi acordăm consultanță juridică.</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-8.jpg" alt="person-8"></figure>
							<h3 class="team-name">Lupu Gheorghe</h3>
							<small class="team-desc">Avocat</small>
							<p>În domeniul dreptului afacerilor va pot acorda consultanță juridică și reprezentare in toate subdomeniile(întocmire, redactarea și expertizarea contractelor comerciale, asistență la negocieri și tranzacții în vederea încheierii contractelor comerciale, etc). </p>
						</div>





<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-9.jpg" alt="person-1"></figure>
							<h3 class="team-name">Raisa Wasylyna</h3>
							<small class="team-desc">Main mechaninc</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor ut tenetur dicta deserunt fugiat itaque voluptates doloremque dolore accusamus suscipit</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-10.jpg" alt="person-2"></figure>
							<h3 class="team-name">George Stevens</h3>
							<small class="team-desc">Painter</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus quisquam hic cum laboriosam, est saepe amet magni labore, corrupti recusandae</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-11.jpg" alt="person-3"></figure>
							<h3 class="team-name">David Smith</h3>
							<small class="team-desc">Engineer</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti quisquam, odit! Mollitia, porro. Quidem officiis, harum tempora, voluptates distinctio quis</p>
						</div>
						<div class="team">
							<figure class="team-image"><img src="<?=BASE_URL?>assets/dummy/person-12.jpg" alt="person-4"></figure>
							<h3 class="team-name">Howarrd Newman</h3>
							<small class="team-desc">Computer Specialist</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae repellat ipsum optio nam! Error at minus fugit est voluptatum, placeat,</p>
						</div>

					</div>
				</div>

			</main> <!-- .main-content -->

			
	<footer class="site-footer">
		<div class="container">
			<div class="subscribe-form">
				<form action="#">
					<label for="#">
						<span>DORIȚI SĂ PRIMI ȘTIRI?</span>
						<span>ÎNSCRIEȚI-VĂ LA STIRELE NOASTRE</span>
					</label>
					<div class="control">
						<input type="text" placeholder="Introduceți adresa de e-mail a  dvs. pentru a vă abona ...">
						<button type="submit"><img src="assets/images/icon-envelope.png" alt=""></button>
				</form>
			</div>
		</div>
		<div class="social-links">
			<a href="<?=BASE_URL?>"><i class="fa fa-facebook"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-twitter"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-google-plus"></i></a>
			<a href="<?=BASE_URL?>"><i class="fa fa-pinterest"></i></a>
		</div>
		<div class="copy">
			<p>Copyright 2021 iLegal Consultation. All rights reserved.</p>
		</div>
		</div>
	</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->

		

		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>

</html>