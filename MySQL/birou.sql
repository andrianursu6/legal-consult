-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 10, 2021 at 09:33 PM
-- Server version: 5.6.43
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `birou`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username_admin` varchar(30) NOT NULL,
  `password_admin` varchar(30) NOT NULL,
  `email_admin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username_admin`, `password_admin`, `email_admin`) VALUES
(1, 'adrian', 'adrian_pass', 'andrianursu6@gmail.com'),
(2, 'costea', 'costea_pass', 'constantn.chihaial@gmail.com'),
(3, 'ion', 'ion_pass', 'ion.zubco@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `avocati`
--

CREATE TABLE `avocati` (
  `id_avocat` int(11) NOT NULL,
  `nume` varchar(30) DEFAULT NULL,
  `prenume` varchar(30) DEFAULT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `adresa` varchar(200) DEFAULT NULL,
  `id_problema` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL,
  `password` varchar(30) NOT NULL DEFAULT 'pass1',
  `email` varchar(50) NOT NULL DEFAULT 'nameofemail@gmail.com'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `avocati`
--

INSERT INTO `avocati` (`id_avocat`, `nume`, `prenume`, `telefon`, `adresa`, `id_problema`, `id_status`, `password`, `email`) VALUES
(1, 'Petran', 'Evdokiya', '+37367601364', 'Chisinau, bd. Cuza-Voda', 2, 2, 'pass1', 'nameofemail4@gmail.com'),
(2, 'Bogdanov', 'Sofron', '+37368236438', 'Chisinau,str. Călușarilor 24', 8, 2, 'pass1', 'nameofemai343l@gmail.com'),
(3, 'Kovalev', 'Virginia', '+37367217236', 'str. Montana 24', 7, 2, 'pass1', 'nameofema3423il@gmail.com'),
(4, 'Cojocaru', 'Demid', '+37367236438', 'str. Petricani 84', 6, 2, 'pass1', 'n212ameofemail@gmail.com'),
(5, 'Popescu', 'Augustin', '+37379510236', 'Chisinau, bd. Cuza-Voda', 5, 2, 'pass1', '23enameofemail@gmail.com'),
(6, 'Ivanov', 'Renat', '+37378448923', 'Chisinau, str. Ion Pruncul 6', 3, 2, 'pass1', '42nameofemail@gmail.com'),
(7, 'Lupu', 'Gheorghe', '+37369373380', 'Chisinau, str. Ivan Zaikin 28/1', 3, 2, 'pass1', '32nameofemail@gmail.com'),
(8, 'Nicolescu', 'Radu', '+37360993299', 'Chisinau, str. Poștei 53', 1, 2, 'pass1', 'tre3@gmail.com'),
(9, 'Ana', 'Sokolov', '+37367473026', 'Chisinau, bd. Cuza-Voda', 5, 2, 'pass1', '4abdo-iisan@gmail.com'),
(10, 'David', 'Ionesco', '+37369819964', 'Chisinau,str. Călușarilor 24', 4, 2, 'pass1', 'kyrabie77u@gmail.com'),
(11, 'Costel', 'Antonov', '+37379242321', 'str. Montana 24', 2, 2, 'pass1', '3fahery@gmail.com'),
(12, 'Inga', 'Krupin', '+37360332111', 'str. Petricani 84', 2, 2, 'pass1', 'bkappy.mahboul@gmail.com'),
(13, 'Sandra', 'Romanov', '+37378269540', 'Chisinau, bd. Cuza-Voda', 1, 2, 'pass1', '0fernandaalmeidak@gmail.com'),
(14, 'Simion', 'Funar', '+37369713839 ', 'Chisinau, str. Ion Pruncul 6', 6, 2, 'pass1', 'imehdi.siimouxi.3@gmail.com'),
(15, 'Mihăiță', 'Chaykovsky', '+37367551018', 'Chisinau, str. Ivan Zaikin 28/1', 7, 2, 'pass1', '9tofahi.anasc@gmail.com'),
(16, 'Grigore', 'Melnik', '+37379308900', 'Chisinau, str. Poștei 53', 8, 2, 'pass1', 'aasking.edition9@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `cerere_neprocesata`
--

CREATE TABLE `cerere_neprocesata` (
  `id_cerere_neprocesata` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_problema` int(11) NOT NULL,
  `descriere_problema` text NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cerere_neprocesata`
--

INSERT INTO `cerere_neprocesata` (`id_cerere_neprocesata`, `id_client`, `id_problema`, `descriere_problema`, `creation_date`) VALUES
(1, 1, 6, 'Asigurarea este achitata dar compania refuza sa achite despagubiri', '2021-06-05 12:21:01'),
(13, 1, 6, 'Asigurarea este achitata dar compania refuza sa achite despagubiri', '2021-06-10 17:53:44'),
(19, 1, 6, 'Asigurarea este achitata dar compania refuza sa achite despagubiri', '2021-06-10 18:12:08');

-- --------------------------------------------------------

--
-- Table structure for table `cereri_procesate`
--

CREATE TABLE `cereri_procesate` (
  `id_cerere` int(11) NOT NULL,
  `id_client` int(11) DEFAULT NULL,
  `id_avocat` int(11) DEFAULT NULL,
  `date_of_procesing` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_problema` int(11) NOT NULL,
  `descriere_problema` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cereri_procesate`
--

INSERT INTO `cereri_procesate` (`id_cerere`, `id_client`, `id_avocat`, `date_of_procesing`, `id_problema`, `descriere_problema`) VALUES
(1, 25, 15, '2021-06-10 05:44:35', 7, 'Aavut loc un accident rutier in care a fost avariat automobilul meu. Compania la care am asigurat automobilul refuza sa plateasca despagubiri.'),
(2, 25, 15, '2021-06-10 05:44:35', 6, 'Asigurarea este achitata dar compania refuza sa achite despagubiri'),
(3, 1, 14, '2021-06-10 16:15:16', 6, 'Asigurarea este achitata dar compania refuza sa achite despagubiri'),
(4, 1, 4, '2021-06-10 17:42:10', 6, 'Asigurarea este achitata dar compania refuza sa achite despagubiri');

-- --------------------------------------------------------

--
-- Table structure for table `clienti`
--

CREATE TABLE `clienti` (
  `id_client` int(11) NOT NULL,
  `nume` varchar(30) DEFAULT NULL,
  `prenume` varchar(30) DEFAULT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `adresa` varchar(200) DEFAULT NULL,
  `password` varchar(30) NOT NULL DEFAULT 'passclient'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clienti`
--

INSERT INTO `clienti` (`id_client`, `nume`, `prenume`, `telefon`, `email`, `adresa`, `password`) VALUES
(1, 'Ivan', 'Tataru', '+37367832018', 'tt.ivan@gmail.com', 'Balti, str. Independentei', 'passclient'),
(25, 'Radu ', 'Cprin', '+37379231843', 'ciprin@gmail.com', 'Comrat, str Stefan cel Mare', 'ciprin1234');

-- --------------------------------------------------------

--
-- Table structure for table `probleme`
--

CREATE TABLE `probleme` (
  `id_problema` int(11) NOT NULL,
  `nume_problema` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `probleme`
--

INSERT INTO `probleme` (`id_problema`, `nume_problema`) VALUES
(1, 'Consultatie'),
(2, 'Servicii publice'),
(3, 'Afceri'),
(4, 'Cauze penale'),
(5, 'Imobiliare'),
(6, 'Recuperatie datorii'),
(7, 'Compensare'),
(8, 'Despagubiri');

-- --------------------------------------------------------

--
-- Table structure for table `status_avocat`
--

CREATE TABLE `status_avocat` (
  `id_status` int(11) NOT NULL,
  `avocat_status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_avocat`
--

INSERT INTO `status_avocat` (`id_status`, `avocat_status`) VALUES
(1, 'ocupat'),
(2, 'liber');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `avocati`
--
ALTER TABLE `avocati`
  ADD PRIMARY KEY (`id_avocat`),
  ADD KEY `id_avocat` (`id_avocat`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_problema` (`id_problema`);

--
-- Indexes for table `cerere_neprocesata`
--
ALTER TABLE `cerere_neprocesata`
  ADD PRIMARY KEY (`id_cerere_neprocesata`),
  ADD KEY `id_cerere_neprocesata` (`id_cerere_neprocesata`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_problema` (`id_problema`);

--
-- Indexes for table `cereri_procesate`
--
ALTER TABLE `cereri_procesate`
  ADD PRIMARY KEY (`id_cerere`),
  ADD KEY `id_cerere` (`id_cerere`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_avocat` (`id_avocat`),
  ADD KEY `id_problema` (`id_problema`);

--
-- Indexes for table `clienti`
--
ALTER TABLE `clienti`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_client_2` (`id_client`),
  ADD KEY `id_client_3` (`id_client`);

--
-- Indexes for table `probleme`
--
ALTER TABLE `probleme`
  ADD PRIMARY KEY (`id_problema`),
  ADD KEY `id_problema` (`id_problema`),
  ADD KEY `id_problema_2` (`id_problema`);

--
-- Indexes for table `status_avocat`
--
ALTER TABLE `status_avocat`
  ADD PRIMARY KEY (`id_status`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_status_2` (`id_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `avocati`
--
ALTER TABLE `avocati`
  MODIFY `id_avocat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cerere_neprocesata`
--
ALTER TABLE `cerere_neprocesata`
  MODIFY `id_cerere_neprocesata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cereri_procesate`
--
ALTER TABLE `cereri_procesate`
  MODIFY `id_cerere` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `clienti`
--
ALTER TABLE `clienti`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `probleme`
--
ALTER TABLE `probleme`
  MODIFY `id_problema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `status_avocat`
--
ALTER TABLE `status_avocat`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `avocati`
--
ALTER TABLE `avocati`
  ADD CONSTRAINT `avocati_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `status_avocat` (`id_status`),
  ADD CONSTRAINT `avocati_ibfk_2` FOREIGN KEY (`id_problema`) REFERENCES `probleme` (`id_problema`);

--
-- Constraints for table `cerere_neprocesata`
--
ALTER TABLE `cerere_neprocesata`
  ADD CONSTRAINT `cerere_neprocesata_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clienti` (`id_client`),
  ADD CONSTRAINT `cerere_neprocesata_ibfk_2` FOREIGN KEY (`id_problema`) REFERENCES `probleme` (`id_problema`);

--
-- Constraints for table `cereri_procesate`
--
ALTER TABLE `cereri_procesate`
  ADD CONSTRAINT `cereri_procesate_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clienti` (`id_client`),
  ADD CONSTRAINT `cereri_procesate_ibfk_2` FOREIGN KEY (`id_avocat`) REFERENCES `avocati` (`id_avocat`),
  ADD CONSTRAINT `cereri_procesate_ibfk_3` FOREIGN KEY (`id_problema`) REFERENCES `probleme` (`id_problema`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
